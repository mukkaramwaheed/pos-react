import React, { Suspense, lazy } from "react"
import { Router, Switch, Route, Redirect } from "react-router-dom"
import { history } from "./history"
import { connect } from "react-redux"
import Spinner from "./components/@vuexy/spinner/Loading-spinner"
import { ContextLayout } from "./utility/context/Layout"

// Route-based code splitting
const Home = lazy(() =>
  import("./views/pages/Home")
)

const Page2 = lazy(() =>
  import("./views/pages/Page2")
)

const login = lazy(() =>
  import("./views/pages/authentication/login/Login")
)

const register = lazy(() =>
  import("./views/pages/authentication/register/Register")
)

// Set Layout and Component Using App Route
const RouteConfig = ({
  component: Component,
  fullLayout,
  permission,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      return (
        <ContextLayout.Consumer>
          {context => {
            let LayoutTag =
              fullLayout === true
                ? context.fullLayout
                : context.state.activeLayout === "horizontal"
                ? context.horizontalLayout
                : context.VerticalLayout
              return (
                <LayoutTag {...props} permission={props.user}>
                  <Suspense fallback={<Spinner />}>
                    <Component {...props} />
                  </Suspense>
                </LayoutTag>
              )
          }}
        </ContextLayout.Consumer>
      )
    }}
  />
)

// const mapStateToProps = state => {
//   return {
//     user: state.auth.login.userRole,
//     isAuthenticated: state.auth.login.isAuthenticated

//   }
// }

// const AppRoute = connect(mapStateToProps)(RouteConfig)
const AppRoute = RouteConfig

class AppRouter extends React.Component {

  // componentDidMount (){
    
  // }
  render() {
    
      console.log("router file " + this.props.isAuthenticated)
      let routes = (
        <Switch>
          <AppRoute
          exact
          path="/"
          component={login}
          fullLayout
          />
          <AppRoute
          path="/register"
          component={register}
          fullLayout
          />
          <Redirect to="/" />
        </Switch>
      )
      if(this.props.isAuthenticated){
        routes = (
          <Switch>
            <AppRoute
              path="/home"
              component={Home}
            />
            <AppRoute
              path="/page2"
              component={Page2}
            />
          </Switch>
        )
      }
    
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        {routes}
      </Router>
    )
  }
}


const mapStateToProps = state => {
  return {
    // user: state.auth.login.userRole,
    isAuthenticated: state.auth.login.isAuthenticated

  }
}

export default connect(mapStateToProps)(AppRouter)
