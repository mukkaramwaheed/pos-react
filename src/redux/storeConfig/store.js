
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk'; 
import rootReducer from "../reducers/rootReducer"

const initialState = {};
const middleware = [thunk];


const store = createStore(
    
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middleware))
)
export  { store };

// import { createStore, applyMiddleware, compose } from "redux"
// import createDebounce from "redux-debounced"
// import thunk from "redux-thunk"
// import rootReducer from "../reducers/rootReducer"
// // import { composeWithDevTools } from "redux-devtools-extensions" 

// const middlewares = [thunk, createDebounce()]

// const  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
// const store = createStore(
//   rootReducer,
//   {},
//   composeEnhancers(applyMiddleware(...middlewares))
// )

// export { store }
