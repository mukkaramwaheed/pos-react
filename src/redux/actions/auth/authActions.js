import { history } from "../../../history"
import axios from "axios"
// import { element } from "prop-types";
import {REGISTER_SUCCESS,
   LOGIN_SUCCESS,
    AUTH_START,
    LOGIN_FAIL,
  USER_LOADED,
AUTH_ERROR} from '../../../utility/types'
import api from '../../../utility/api';
import setAuthToken from '../../../utility/setAuthToken';


export const loadUser = () => async dispatch => {
  try {


    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }

    const res = await api.get('auth');

    dispatch({
      type: USER_LOADED,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: AUTH_ERROR
    });
  }
};

export const authStart = () => {
  return {
      type: AUTH_START
  };
};

export const signupWithJWT = (data) => async dispatch => {
  try {

    //remove all the errors first
    dispatch({
      type: "ERROR_CLEAR",
      payload: ''
    })

    const res = await axios.post('api/v1/auth/register', data);

    dispatch({
      type: REGISTER_SUCCESS,
      payload: res.data
    });

    // history.push("/home")
    
  } catch (err) {

    if(err){
      dispatch({
        type: "REGISTER_ERROR",
        payload: err.response.data.error
      })
    }
  }
};

export const login = (data) => async dispatch => {
  try {

    dispatch(authStart());
    const res = await axios.post('api/v1/auth/login', data);

    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data
    });

    history.push("/home")
    
  } catch (err) {

    dispatch({
      type: LOGIN_FAIL,
      payload: err.response.data.error
    })

  }
};
