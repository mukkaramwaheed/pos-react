import { AUTH_START,
  USER_LOADED,
  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
LOGIN_FAIL,
ERROR_CLEAR } from '../../../utility/types';

export const login = (state = {
  token: localStorage.getItem('token'),
  isAuthenticated: null,
  loading: true,
  user: null
},
  action) => {
  switch (action.type) {
    case AUTH_START:
      return {
        ...state,
        isAuthenticated: false,
        loading: false,
        error: ''
      }
    case USER_LOADED:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        user: action.payload
      }
    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS: {

      return {
        ...state,
        isAuthenticated: true,
        loading: false
      };
    }
    case "REGISTER_ERROR":
    case LOGIN_FAIL:
      {
        return {
          ...state,
          loginError: action.payload
        }
      }
    case ERROR_CLEAR: {
      return {
        ...state,
        error: ''
      }
    }
    default: {
      return state
    }
  }
}
