
import React from "react"
import {
  Button,
  FormGroup
} from "reactstrap"
import { Formik, Field, Form, ErrorMessage } from "formik"
import * as Yup from "yup"
import { history } from "../../../../history"
import { connect } from "react-redux"
import { signupWithJWT } from "../../../../redux/actions/auth/authActions"
// import { Alert } from '../../../../components/reactstrap/alerts/Alerts'
import {
  Alert
} from "reactstrap"

const SignUpSchema = Yup.object().shape({
  email: Yup.string()
    .email("Invalid email address")
    .required("Required"),
  name: Yup.string()
    .min(2, "Must be longer than 2 characters")
    .max(20, "Nice try, nobody has a first name that long")
    .required("Required"),
  password: Yup.string()
    .min(6, "Must be longer than 6 characters")
    .required("Required"),
  confirmpassword: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords and confirm password not match')
    .min(6, "Must be longer than 6 characters")
    .required("Required"),

})

class RegisterJWT extends React.Component {

  render() {

    // let validationError = '1234'

    return (

      <Formik
        initialValues={{
          email: "test@gmail.com",
          name: "test",
          password: "123456",
          confirmpassword: "123456"
        }}
        validationSchema={SignUpSchema}
        // validateOnBlur="true"
        // enableReinitialize={formValues }

        onSubmit={async (values, { setSubmitting, setFieldError }) => {

          let data = {
            'name': values.name,
            'email': values.email,
            'password': values.password
          }
          await this.props.signupWithJWT(data);
          let formErrors = this.props.serverError;

          if (Array.isArray(formErrors)) {
            formErrors.forEach(error =>
              setFieldError(Object.keys(error), Object.values(error))

            );
          }
          else {
            console.log(this.props.isAuthenticated);
            if (this.props.isAuthenticated) {
              history.push('/home')
            }
          }

          setSubmitting(false);
        }}

      >
        {props => {
          console.log(props.errors)
          return (

            <Form>
              {(!Array.isArray(this.props.serverError) && this.props.serverError !== '') ? <Alert color="danger">{this.props.serverError}</Alert> : ''}

              <FormGroup>
                <label htmlFor="name">Name</label>
                <Field
                  className="form-control"
                  name="name"
                  placeholder="Jane"
                  type="text"
                />
                <ErrorMessage
                  name="name"
                  component="div"
                  className="field-error text-danger"
                />
              </FormGroup>
              <FormGroup>
                <label htmlFor="email">Email</label>
                <Field
                  className="form-control"
                  name="email"
                  placeholder="jane@acme.com"
                  type="email"
                />

                <ErrorMessage name="email">
                  {(msg) => (
                    <div className="field-error text-danger">{msg}</div>
                  )}
                </ErrorMessage>
              </FormGroup>
              <FormGroup>
                <label htmlFor="password">Password</label>
                <Field
                  className="form-control"
                  name="password"
                  placeholder="jane@acme.com"
                  type="password"
                />
                {/* This will render a string */}
                <ErrorMessage name="password">
                  {(msg /** this is the same as the above */) => (
                    <div className="field-error text-danger">{msg}</div>
                  )}
                </ErrorMessage>
              </FormGroup>
              <FormGroup>
                <label htmlFor="confirmpassword">Confirm Password</label>
                <Field
                  className="form-control"
                  name="confirmpassword"
                  placeholder="jane@acme.com"
                  type="password"
                />
                {/* This will render a string */}
                <ErrorMessage name="confirmpassword">
                  {(msg /** this is the same as the above */) => (
                    <div className="field-error text-danger">{msg}</div>
                  )}
                </ErrorMessage>
              </FormGroup>
              <div className="d-flex justify-content-between">
                <Button.Ripple
                  color="primary"
                  outline
                  onClick={() => {
                    history.push("/")
                  }}
                >
                  Login
          </Button.Ripple>
                <Button.Ripple color="primary" disabled={!(props.isSubmitting || props.isValid)} type="submit">
                  Register
          </Button.Ripple>

              </div>
            </Form>
          )
        }}
      </Formik>
    )
  }
}

const mapStateToProps = (state) => ({
  serverError: state.auth.login.error ?? '',
  isAuthenticated: state.auth.login.isAuthenticated
})

// export default connect(null,{signupWithJWT})(RegisterJWT)
export default connect(mapStateToProps, { signupWithJWT })(RegisterJWT)

