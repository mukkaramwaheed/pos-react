import React from "react"
// import { Redirect } from 'react-router-dom';

import {
  Button,
  Card,
  CardBody,
  Row,
  Col,
  FormGroup
} from "reactstrap"
import loginImg from "../../../../assets/img/pages/login.png"
import "../../../../assets/scss/pages/authentication.scss"
import { Formik, Field, Form, ErrorMessage } from "formik"
import * as Yup from "yup"
import { history } from "../../../../history"
import { connect } from "react-redux"
import { login } from "../../../../redux/actions/auth/authActions"
import {
  Alert
} from "reactstrap"

const LoginSchema = Yup.object().shape({
  email: Yup.string()
    .email("Invalid email address")
    .required("Required"),
  password: Yup.string()
    .min(6, "Must be longer than 6 characters")
    .required("Required")
})

class Login extends React.Component {
  render() {
    return (
      
      <Row className="m-0 justify-content-center">
       
        <Col
          sm="8"
          xl="7"
          lg="10"
          md="8"
          className="d-flex justify-content-center"
        >
          <Card className="bg-authentication login-card rounded-0 mb-0 w-100">
            <Row className="m-0">
              <Col
                lg="6"
                className="d-lg-block d-none text-center align-self-center px-1 py-0"
              >
                <img src={loginImg} alt="loginImg" />
              </Col>
              <Col lg="6" md="12" className="p-0">
                <Card className="rounded-0 mb-0 px-2">
                  <CardBody>
                    <h4>Login</h4>
                    <p>Welcome back, please login to your account.</p>

                    <Formik
                      initialValues={{
                        email: "test@gmail.com",
                        password: "123456",
                      }}
                      validationSchema={LoginSchema}

                      onSubmit={async (values, { setSubmitting, setFieldError }) => {

                        let data = {
                          'email': values.email,
                          'password': values.password
                        }
                        this.props.login(data);
                        let formErrors = this.props.serverError;

                        if (Array.isArray(formErrors)) {
                          formErrors.forEach(error =>
                            setFieldError(Object.keys(error), Object.values(error))
                          );
                        }
                        
                        setSubmitting(false);
                      }}

                    >
                      {props => {
                        return (                        
                          <Form>
                            {(!Array.isArray(this.props.serverError) && this.props.serverError !== '') ? 
                            <Alert color="danger">{this.props.serverError}</Alert> : ''}

                            <FormGroup>
                              <label htmlFor="email">Email</label>
                              <Field
                                className="form-control"
                                name="email"
                                placeholder="jane@acme.com"
                                type="email"
                              />

                              <ErrorMessage name="email">
                                {(msg) => (
                                  <div className="field-error text-danger">{msg}</div>
                                )}
                              </ErrorMessage>
                            </FormGroup>
                            <FormGroup>
                              <label htmlFor="password">Password</label>
                              <Field
                                className="form-control"
                                name="password"
                                placeholder="jane@acme.com"
                                type="password"
                              />
                              {/* This will render a string */}
                              <ErrorMessage name="password">
                                {(msg /** this is the same as the above */) => (
                                  <div className="field-error text-danger">{msg}</div>
                                )}
                              </ErrorMessage>
                            </FormGroup>

                            <div className="d-flex justify-content-between">
                              <Button.Ripple
                                color="primary"
                                outline
                                onClick={() => {
                                  history.push("/register")
                                }}
                              >
                                Register
                              </Button.Ripple>
                              <Button.Ripple color="primary" disabled={!(props.isSubmitting || props.isValid)} type="submit">
                                Login
                              </Button.Ripple>

                            </div>
                          </Form>
                        )
                      }}
                    </Formik>
                  </CardBody>

                </Card>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    )
  }
}

const mapStateToProps = (state) => ({
  serverError: state.auth.login.loginError ?? '',
  isAuthenticated: state.auth.login.isAuthenticated
})

export default connect(mapStateToProps, { login })(Login)
